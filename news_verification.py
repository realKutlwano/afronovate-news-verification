# Import necessary libraries
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split 
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, classification_report
import pandas as pd
import pickle

# Load data 
data = pd.read_csv("news.csv")

# Get data shape
print(data.shape)

# See what the dataset looks like
print(data.head())


# Filter labels
x = data['text']
y = data['label']

# Split the data into train and validation set
x_train, x_val, y_train, y_val = train_test_split(x, y, test_size=0.2, stratify=y)

#Creating a pipeline that first creates bag of words(after applying stopwords) & then applies Multinomial Naive Bayes model
pipeline = Pipeline([('tfidf', TfidfVectorizer(stop_words='english')),
                    ('nbmodel', MultinomialNB())])


#Training our data
pipeline.fit(x_train, y_train)

#Predicting the label for the test data
pred = pipeline.predict(x_val)

#Checking the performance of our model
print(classification_report(y_val, pred))
print(confusion_matrix(y_val, pred))

# Predict and calculate accuracy
y_pred=pipeline.predict(x_val)
score=accuracy_score(y_val,y_pred)
print(f'Accuracy: {round(score*100,2)}%')

# Confusion Matrix
print(confusion_matrix(y_val, y_pred, labels=['FAKE', 'REAL']))

# Save model
with open("news_verifier.pickle", 'wb') as file:
    pickle.dump(pipeline, file)