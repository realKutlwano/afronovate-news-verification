# Import libraries
import streamlit as st 
import newspaper
from newspaper import Article
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle
import urllib

# Load model
model = pickle.load(open("news_verifier.pickle", "rb"))

# Set app title
st.title("News Verification app")

# Take article url
st.write("Please copy and paste article url here: ")
url = st.text_input("")

# Submit and return submission
if st.button("Submit"):
    url = urllib.parse.unquote(url)
    article = Article(str(url))
    article.download()
    article.parse()
    article.nlp()
    news = article.summary
    pred = model.predict([news])
    if pred == ['FAKE']:
        st.write("This news is 'FAKE'")
    elif pred == ['REAL']:
        st.write("This news is 'REAL'")